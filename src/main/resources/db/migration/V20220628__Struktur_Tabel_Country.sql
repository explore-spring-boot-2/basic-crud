CREATE TABLE Country(
	code VARCHAR(2)
	, latitude DOUBLE
	, longitude DOUBLE
	, name VARCHAR(60)
	, CONSTRAINT pk_country PRIMARY KEY (code)
);