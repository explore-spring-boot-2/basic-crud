package id.lab.dao;

import id.lab.entity.Country;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CountryDao extends PagingAndSortingRepository<Country, String> {
}
