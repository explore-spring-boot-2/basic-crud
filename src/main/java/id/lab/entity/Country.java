package id.lab.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Country {
    @Id
    private String code;
    private String name;
    private Double latitude;
    private Double longitude;
}
