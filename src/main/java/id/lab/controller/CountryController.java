package id.lab.controller;

import id.lab.dao.CountryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/country")
public class CountryController {

    @Autowired
    CountryDao countryDao;

    @GetMapping("/index")
    public String index(HttpServletRequest request, Model model) {
        model.addAttribute("countries", countryDao.findAll());
        return("country/index");
    }
}
