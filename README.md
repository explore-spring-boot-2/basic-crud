# Basic CRUD

Belajar dasar CRUD (Create Read Update Delete) menggunakan spring boot web, flywaydb (database-migration tool) dan thymeleaf (template engine)

## Requirement 

`JAVA_HOME` sudah disetting dengan benar

## Cara menjalankan

* windows `mvnw.cmd spring-boot:run` 
* linux `./mvnw spring-boot:run`

